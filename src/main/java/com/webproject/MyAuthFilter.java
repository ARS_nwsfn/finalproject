package com.webproject;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

@Component("myAuthFilter")
public class MyAuthFilter extends UsernamePasswordAuthenticationFilter {

    private int errCode = 5;

    @Autowired
    @Qualifier("authenticationManager")
    @Override
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
    }


    
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {

        logger.info("running my own version of UsernamePasswordFilter");
        
        String login = (String) request.getParameter("login");
        String password = (String) request.getParameter("password");
        errCode = validate(login, password);
        if (login.isEmpty() || password.isEmpty()) {
            try {
                errCode = validate(login, password);
                logger.warn("Error while logging secured");
                response.sendRedirect("/login?error=" + errCode);
            } catch (IOException ex) {
                Logger.getLogger(MyAuthFilter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(login, password);
        // Allow subclasses to set the "details" property
        setDetails(request, authRequest);
        return this.getAuthenticationManager().authenticate(authRequest);
    }

    private int validate(String login, String password) {

        if (login.isEmpty() && password.isEmpty()) {
            return 4;
        }
        if (login.isEmpty() && !password.isEmpty()) {
            return 2;
        }
        if (!login.isEmpty() && password.isEmpty()) {
            return 3;
        }

        return 1;
    }
}
