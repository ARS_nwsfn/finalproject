package com.webproject.entities;

import com.webproject.entities.Courses;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "COURSE_APPROVE")
public class CourseApprove {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "approval_id")
    private int approval_id;

    @Column(name = "approvedby_km")
    private String approvedBy_km = "false";

    @Column(name = "km_reason")
    private String km_reason;

    @Column(name = "approvedby_dm")
    private String approvedBy_dm = "false";

    @Column(name = "dm_reason")
    private String dm_reason;

    @OneToOne
    @PrimaryKeyJoinColumn
    private Courses courses;

    public Courses getCourse() {
        return courses;
    }

    public void setCourse(Courses course) {
        this.courses = course;
    }

    public void setApproval_id(int approval_id) {
        this.approval_id = approval_id;
    }

    public void setApprovedBy_km(String approvedBy_km) {
        this.approvedBy_km = approvedBy_km;
    }

    public void setKm_reason(String km_reason) {
        this.km_reason = km_reason;
    }

    public void setApprovedBy_dm(String approvedBy_dm) {
        this.approvedBy_dm = approvedBy_dm;
    }

    public void setDm_reason(String dm_reason) {
        this.dm_reason = dm_reason;
    }

    public int getApproval_id() {
        return approval_id;
    }

    public String getApprovedBy_km() {
        return approvedBy_km;
    }

    public String getKm_reason() {
        return km_reason;
    }

    public String getApprovedBy_dm() {
        return approvedBy_dm;
    }

    public String getDm_reason() {
        return dm_reason;
    }

    public CourseApprove() {

    }

}
