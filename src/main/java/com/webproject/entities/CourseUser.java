package com.webproject.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "COURSE_USERS",
    uniqueConstraints = {@UniqueConstraint(columnNames = {"course_name", "user_name"})})
public class CourseUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ev_id")
    private int ev_id;

    
    @Column(name = "course_name", nullable = false)
    private String courseName;
    
    @Column(name = "user_name", nullable = false)
    private String userName;
    
    @Column(name = "course_role")
    private String courseRole;
    
    @Column(name = "grade")
    private int grade ;
    
    @ManyToOne
    @JoinColumn(name = "course_id")
    private Courses course;

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public Courses getCourse() {
        return course;
    }

    public void setCourse(Courses course) {
        this.course = course;
    }
     
    
    public void setCourseRole(String courseRole) {
        this.courseRole = courseRole;
    }

    public String getCourseRole() {
        return courseRole;
    }

    public int getCourse_id() {
        return ev_id;
    }

    public String getCourseName() {
        return courseName;
    }

    public String getUserName() {
        return userName;
    }

    public void setCourse_id(int course_id) {
        this.ev_id = course_id;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public CourseUser(){
        
    }

    @Override
    public String toString() {
        return "CourseUser{" + "ev_id=" + ev_id + ", courseName=" + courseName + ", userName=" + userName + ", courseRole=" + courseRole + ", grade=" + grade + '}';
    }

    


    
}
