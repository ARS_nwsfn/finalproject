package com.webproject.entities;

public enum State {
    NEW, READY, OPEN, INPROGRESS, FINISHED, PROPOSAL, REVIEW, DRAFT, REJECT
}
