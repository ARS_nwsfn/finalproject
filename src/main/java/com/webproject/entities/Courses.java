package com.webproject.entities;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.springframework.beans.factory.annotation.Autowired;

@Entity
@Table(name = "COURSES")
public class Courses{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "course_id")
    private int course_id;

    @Column(name = "course_name", nullable = false)
    private String courseName;

    @Column(name = "course_owner", nullable = false)
    private String course_owner;

    @Column(name = "course_description")
    private String courseDescription;

    @Column(name = "course_links")
    private String links;
    
    @Column(name = "course_category")
    private String category;
    
    private final State DEFAULT_STATE = State.DRAFT;
    
    @Column(name = "course_state")
    @Enumerated(EnumType.STRING)
    private State state = DEFAULT_STATE;
    
    @Column(name = "minSubs")
    private Integer minSubs = 1;
    
    @Column(name = "minAtts")
    private Integer minAtts = 1;
    
    @Autowired
    @OneToOne(mappedBy = "courses", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private CourseApprove approve = new CourseApprove();
    
    @Autowired
    @OneToMany(mappedBy = "course", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<CourseUser> courseUsers = new HashSet<>(0);

    public Set<CourseUser> getCourseUsers() {
        return courseUsers;
    }

    public void setCourseUsers(Set<CourseUser> courseUsers) {
        this.courseUsers = courseUsers;
    }
    
    

    public Integer getMinAtts() {
        return minAtts;
    }

    public void setMinAtts(Integer minAtts) {
        this.minAtts = minAtts;
    }

    public int getMinSubs() {
        return minSubs;
    }

    public void setMinSubs(int minSubs) {
        this.minSubs = minSubs;
    }

    public CourseApprove getApprove() {
        return approve;
    }

    public void setApprove(CourseApprove approve) {
        this.approve = approve;
    }

    
    
    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    
    
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    
    
    public int getId() {
        return course_id;
    }


    public String getCourseName() {
        return courseName;
    }

    public String getOwner() {
        return course_owner;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public String getLinks() {
        return links;
    }

    /// constructor
    public Courses(String courseName, String owner, String courseDescription, String links, String category) {
        this.courseName = courseName;
        this.course_owner = owner;
        this.courseDescription = courseDescription;
        this.links = links;
        this.category = category;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    public void setOwner(String author) {
        this.course_owner = author;
    }

    public void setLinks(String links) {
        this.links = links;
    }

    public void setId(int id) {
        this.course_id = id;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Courses() {

    }

    @Override
    public String toString() {
        return "Courses{" + "course_id=" + course_id + ", courseName=" + courseName + 
                ", course_owner=" + course_owner + ", courseDescription=" + courseDescription + ", links=" + links + 
                ", category=" + category  + ", state=" + state + ", minSubs=" + minSubs + ", minAtts=" + minAtts + 
                ", approve=" + approve + ", courseUsers=" + courseUsers + "}";
    }

   

    

    public String courseContains(String name) {

        String role = "new";
        for (CourseUser item : courseUsers) {
            if (/*item.getCourseName() == courseName &&*/ item.getUserName().equals(name)) {
                role = item.getCourseRole();
            }
        }
        return role;
    }

    public int numberOfSubs(String role) {
        int subs = 0;
        for (CourseUser item : courseUsers) {
            if (role.equals(item.getCourseRole())) {
                subs++;
            }
        }
        return subs;
    }

    public double averageGrade() {
        double grade = 0;
        int number = 0;
        for (CourseUser item : courseUsers) {
            if (item.getGrade() != 0){
                number++;
            }
            grade+= item.getGrade();
            
        }
        if (number!= 0){
            return grade/number;
        }
        return grade;
    }

}
