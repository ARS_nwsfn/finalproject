package com.webproject.Services;

import com.webproject.DAOservices.CoursesDAO;
import com.webproject.DAOservices.UserDAO;
import com.webproject.email.Notifies;
import com.webproject.entities.CourseApprove;
import com.webproject.entities.Courses;
import com.webproject.entities.State;
import com.webproject.entities.User;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;

@Configuration
public class Approve {

    @Autowired
    Notifies nf;
    
    @Autowired
    CourseWork cw;
    
    @Autowired
    UserDAO userDAO;
    
    @Autowired
    CoursesDAO courseDAO;

    public void approve(Authentication auth, Courses course, String dec, String reason) {
        ArrayList<String> roles = cw.getRoles(auth);
        CourseApprove approve = course.getApprove();
        if (approve == null) {
            approve = new CourseApprove();
        }
        if (dec.equals("Reject")) {
            course.setState(State.REJECT);
            nf.notifyLecturerRejected(course);
        }

        if (roles.contains("ROLE_DEPARTMENT_MANAGER")) {
            approve.setApprovedBy_dm(dec);
            approve.setDm_reason(reason);
            nf.notifyLecturer(auth.getName(), course, dec, reason);
        }
        if (roles.contains("ROLE_KNOWLEDGE_MANAGER")) {
            approve.setApprovedBy_km(dec);
            approve.setKm_reason(reason);
            nf.notifyLecturer(auth.getName(), course, dec, reason);
        }
        if (approve.getApprovedBy_dm().equals("Approve") && approve.getApprovedBy_km().equals("Approve")) {
            course.setState(State.NEW);
            List<User> users = userDAO.getUsers();
            for (User item : users) {
                nf.notifyUserCourseCreate(course, item.getEmail());
            }
        }
        course.setApprove(approve);
        courseDAO.update(course);
    }

}
