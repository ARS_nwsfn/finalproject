package com.webproject.Services;

import com.webproject.DAOservices.CourseUserDAO;
import com.webproject.entities.CourseApprove;
import com.webproject.entities.CourseUser;
import com.webproject.entities.Courses;
import com.webproject.DAOservices.CoursesDAO;
import com.webproject.DAOservices.UserDAO;
import com.webproject.entities.User;
import com.webproject.email.Notifies;
import com.webproject.entities.State;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@Configuration
public class CourseWork {

    @Autowired
    UserDAO userDAO;

    @Autowired
    Notifies nf;

    @Autowired
    CoursesDAO courseDAO;

    @Autowired
    CourseUserDAO courseUserDAO;

    // here we validate data that lector entered when created or updated the course
    //for each data complies each error: 1 for name , 3 for description and 5 for category
    public String validateUpdateData(String name, String desc, String category) {
        String err = "";
        if (name.isEmpty()) {
            err += "1";
        }
        if (desc.isEmpty()) {
            err += "3";
        }
        if (category.isEmpty()) {
            err += "5";
        }
        return err;
    }

    public void update(int id, String name, String desc, String links, String category, int subs) {

        Courses course = courseDAO.getCourse(id);
        course.setCourseName(name);
        course.setCourseDescription(desc);
        course.setLinks(links);
        course.setCategory(category);
        course.setMinSubs(subs);
        courseDAO.update(course);
    }

    public void update(Courses course) {
        courseDAO.update(course);
    }

    public void changeState(Courses course, State state) {
        course.setState(state);
        update(course);
    }

    public ArrayList<String> getRoles(Authentication auth) {

        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) auth.getAuthorities();
        ArrayList<String> list = new ArrayList<>();
        for (SimpleGrantedAuthority item : authorities) {
            list.add(item.toString());
        }
        return list;
    }

    public void subscribe(int id, String name, String role) {

        Courses course = courseDAO.getCourse(id);
        Set<CourseUser> courseUsers = course.getCourseUsers();
        CourseUser user = courseUserDAO.getCourseUser(course, name);
        if (user == null) {
            CourseUser newUser = new CourseUser();
            newUser.setUserName(name);
            newUser.setCourseRole(role);
            newUser.setCourseName(course.getCourseName());
            newUser.setCourse(course);
            courseUsers.add(newUser);
            course.setCourseUsers(courseUsers);
            update(course);
        } else {
            user.setCourseRole(role);
            courseUserDAO.updateCourseUser(user);
        }
    }

    public int validateGrade(String grade) {

        int b;
        try {
            b = Integer.parseInt(grade);
        } catch (NumberFormatException e) {
            b = -1;
        }
        return b;
    }

    public boolean setGrade(int id, String name, int grade) {

        boolean err = false;
        Courses course = courseDAO.getCourse(id);
        CourseUser user = courseUserDAO.getCourseUser(course, name);
        if (user.getGrade() == 0) {
            err = true;
            user.setGrade(grade);
            courseUserDAO.updateCourseUser(user);
        }
        return err;
    }

    public int getGrade(Courses course, String name) {
        int grade = 0;
        Set<CourseUser> courseUsers = course.getCourseUsers();
        for (CourseUser item : courseUsers) {
            if (item.getUserName() == name) {
                grade = item.getGrade();
            }
        }

        return grade;
    }

    public List<Courses> sort(List<Courses> crs, String category) {

        for (Iterator<Courses> iterator = crs.iterator(); iterator.hasNext();) {
            Courses course = iterator.next();
            if (category.toLowerCase().equals("all")) {
                return crs;
            } else if (!course.getCategory().equals(category)) {
                iterator.remove();
            }
        }
        Collections.sort(crs, (Courses a1, Courses a2) -> a1.getId() - a2.getId());
        return crs;
    }

    public List<Courses> getCourses(Authentication auth) {
        String name = auth.getName();
        ArrayList<String> roles = getRoles(auth);
        List<Courses> courses = new ArrayList<>();
        for (String role : roles) {
            switch (role) {
                case "ROLE_USER": {
                    courses.addAll(courseDAO.getCoursesByState("READY"));
                    courses.addAll(courseDAO.getCoursesByState("NEW"));
                    courses.addAll(courseDAO.getCoursesByState("OPEN"));
                    courses.addAll(courseDAO.getCoursesByState("INPROGRESS"));
                    courses.addAll(courseDAO.getCoursesByState("FINISHED"));
                }
                break;
                case "ROLE_DEPARTMENT_MANAGER": {

                    courses.addAll(courseDAO.getCoursesByState("PROPOSAL"));
                }
                break;
                case "ROLE_KNOWLEDGE_MANAGER": {

                    courses.addAll(courseDAO.getCoursesByState("PROPOSAL"));
                }
                break;
                case "ROLE_LECTURER": {
                    courses.addAll(courseDAO.getCoursesByState("REVIEW", name));
                    courses.addAll(courseDAO.getCoursesByState("DRAFT", name));
                    courses.addAll(courseDAO.getCoursesByState("REJECT", name));
                }
                break;

            }
        }
        Collections.sort(courses, (Courses a1, Courses a2) -> a1.getId() - a2.getId());
        return courses;
    }

    public List<Courses> getUserCourses(Authentication auth) {
        List<Courses> courses = courseDAO.getCourses();
        for (Iterator<Courses> iterator = courses.iterator(); iterator.hasNext();) {
            Courses course = iterator.next();
            Set<CourseUser> users = course.getCourseUsers();
            int i = 0;
            for (CourseUser item : users) {
                if (item.getUserName().equals(auth.getName())) {
                    i = 1;
                }
            }
            if (i == 0 && !course.getOwner().equals(auth.getName())) {
                iterator.remove();
            }
        }
        return courses;
    }

}
