package com.webproject.email;

import com.webproject.DAOservices.CourseUserDAO;
import com.webproject.DAOservices.CoursesDAO;
import com.webproject.DAOservices.UserDAO;
import com.webproject.entities.Courses;
import com.webproject.entities.User;
import com.webproject.entities.CourseUser;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Notifies {

    @Autowired
    UserDAO userDAO;
    
    @Autowired
    CoursesDAO courseDAO;
    
    @Autowired
    CourseUserDAO courseUserDAO;

    @Autowired
    MailMail mm;
    
    public void sendMail(String from, String to, String title, String message) {
        mm.sendMail(from,
                to,
                title,
                message);
    }
    
    //  notify manager about course for review
     public void notifyManager(String from, Courses course, String manager) {
        User user = userDAO.getUserByRole(manager);
        String email = user.getEmail();
        String text
                = course.getOwner() + " had created a new course\n"
                + "----------------------------------------------------\n"
                + "Name: " + course.getCourseName() + "\n"
                + "Category: " + course.getCategory() + "\n"
                + "Description: " + course.getCourseDescription() + "\n"
                + "Links: " + course.getLinks() + "\n"
                + "----------------------------------------------------\n"
                + "Please, review this course at " + "http://localhost:8080/courses/" + course.getId() + "/approve";
        sendMail(from, email, "Course Announcement", text);
    }
     // notify lecturer about managers decision
    public void notifyLecturer(String name, Courses course, String dec, String reason) {
        User lecturer = userDAO.getUserByName(course.getOwner());
        User manager = userDAO.getUserByName(name);
        String text
                = manager.getUsername() + " had made a decision\n"
                + "----------------------------------------------------\n"
                + "Name: " + course.getCourseName() + "\n"
                + "Lecturer: " + course.getOwner() + "\n"
                + "----------------------------------------------------\n"
                + "Decision: " + dec + "\n"
                + "Reason: " + reason + "\n";
        sendMail(lecturer.getEmail(), manager.getEmail(), "Course Approval Update", text);
    }
    // notifiy user about created course
    public void notifyUserCourseCreate(Courses course, String email) {
        User lecturer = userDAO.getUserByName(course.getOwner());
        String text
                = "New course had been added by " + course.getOwner() + "\n"
                + "----------------------------------------------------\n"
                + "Name: " + course.getCourseName() + "\n"
                + "Category: " + course.getCategory() + "\n"
                + "Description: " + course.getCourseDescription() + "\n"
                + "Links: " + course.getLinks() + "\n"
                + "----------------------------------------------------\n"
                + "See course details " + "http://localhost:8080/courses/" + course.getId()
                + "Or became a subscriber  " + "http://localhost:8080/courses/" + course.getId() + "/subscribe";
        sendMail(lecturer.getEmail(), email, "New Course Added", text);
    }
    // notify user and a lecturer when course passed to 'open' state
    public void notifyUserCourseOpen(Courses course) {
        User user = userDAO.getUserByName(course.getOwner());
        String text
                = "course is now open "  + "\n"
                + "----------------------------------------------------\n"
                + "Name: " + course.getCourseName() + "\n"
                + "Category: " + course.getCategory() + "\n"
                + "Description: " + course.getCourseDescription() + "\n"
                + "Links: " + course.getLinks() + "\n"
                + "----------------------------------------------------\n"
                + "See course details " + "http://localhost:8080/courses/" + course.getId()
                + "Or became an attendee  " + "http://localhost:8080/courses/" + course.getId() + "/attend";
        sendMail("our courses", user.getEmail(), "New Course Added", text);
        Set<CourseUser> users = course.getCourseUsers();
        for (CourseUser item : users){
            user = userDAO.getUserByName(item.getUserName());
            sendMail("our courses", user.getEmail(), "Course is open", text);
        }
    }
   
// notify lecturer that course reached min number of attendees    
     public void notifyLectyrerCourseReady(Courses course) {
        User user = userDAO.getUserByName(course.getOwner());
        String text
                = "course is now ready "  + "\n"
                + "----------------------------------------------------\n"
                + "Name: " + course.getCourseName() + "\n"
                + "Category: " + course.getCategory() + "\n"
                + "Description: " + course.getCourseDescription() + "\n"
                + "Links: " + course.getLinks() + "\n"
                + "----------------------------------------------------\n"
                + "See course details " + "http://localhost:8080/courses/" + course.getId();
        sendMail("courses", user.getEmail(), "course is ready", text);
    }

    // notify lecturer about rejected course
    public void notifyLecturerRejected(Courses course) {
        User lecturer = userDAO.getUserByName(course.getOwner());
        int vote = 0;
        if (course.getApprove().getApprovedBy_dm().equals("reject")) {
            vote++;
        }
        if (course.getApprove().getApprovedBy_km().equals("reject")) {
            vote++;
        }
        String voteText = "one vote";
        if (vote == 2) {
            voteText = "both";
        }
        String text
                = "Course was rejected by " + voteText + "\n"
                + "----------------------------------------------------\n"
                + "Decision: " + course.getApprove().getApprovedBy_dm() + "\n"
                + "Reason: " + course.getApprove().getDm_reason() + "\n"
                + "----------------------------------------------------\n"
                + "Decision: " + course.getApprove().getApprovedBy_km() + "\n"
                + "Reason: " + course.getApprove().getKm_reason() + "\n";
        sendMail("managers", lecturer.getEmail(), "Course Rejected", text);
    }

    //notify managers that course was deleted
    public void notifyManagersDeleted(Courses course) {
        User manager1 = userDAO.getUserByRole("ROLE_KNOWLEDGE_MANAGER");
        User manager2 = userDAO.getUserByRole("ROLE_DEPARTMENT_MANAGER");
        String text
                = "Course deleted by  " + course.getOwner() + "\n"
                + "----------------------------------------------------\n"
                + "Name: " + course.getCourseName() + "\n"
                + "Category: " + course.getCategory() + "\n"
                + "Description: " + course.getCourseDescription() + "\n"
                + "Links: " + course.getLinks() + "\n";
        if( !course.getApprove().getApprovedBy_dm().equals("false") || !course.getApprove().getApprovedBy_km().equals("false")){
            text += "----------------------------------------------------\n"
                + "Previously course was rejected by the following votes:" + "\n"
                + "----------------------------------------------------\n"
                + "Decision: " + course.getApprove().getApprovedBy_dm() + "\n"
                + "Reason: " + course.getApprove().getDm_reason() + "\n"
                + "----------------------------------------------------\n"
                + "Decision: " + course.getApprove().getApprovedBy_km() + "\n"
                + "Reason: " + course.getApprove().getKm_reason() + "\n";
        }
        sendMail("km", manager1.getEmail(), "Course Rejected", text);
        sendMail("dm", manager2.getEmail(), "Course Rejected", text);
    }
    
     // notify attendees  when course is started or fibished
    public void notifyAttendees(String message, Courses course) {
        String text
                = message  + "\n"
                + "----------------------------------------------------\n"
                + "Name: " + course.getCourseName() + "\n"
                + "Category: " + course.getCategory() + "\n"
                + "Description: " + course.getCourseDescription() + "\n"
                + "Links: " + course.getLinks() + "\n"
                + "----------------------------------------------------\n"
                + "See course details " + "http://localhost:8080/courses/" + course.getId();
        List<CourseUser> courseUsers = new ArrayList<>();
        courseUsers.addAll(course.getCourseUsers());
        for (CourseUser item : courseUsers){
            if (item.getCourseRole().equals("attendee")){
                User user = userDAO.getUserByName(item.getUserName());
            sendMail("lecturer", user.getEmail(), message, text);
            }
        }
    }
    
      // notify attendees  to evaluate the course
    public void notifyAttendeesForEval( Courses course) {
        String text
                = "please evaluate the course"  + "\n"
                + "----------------------------------------------------\n"
                + "Name: " + course.getCourseName() + "\n"
                + "Category: " + course.getCategory() + "\n"
                + "Description: " + course.getCourseDescription() + "\n"
                + "Links: " + course.getLinks() + "\n"
                + "----------------------------------------------------\n"
                + "See course details " + "http://localhost:8080/courses/" + course.getId()
                + "Evaluate course here " + "http://localhost:8080/courses/" + course.getId() + "/evaluate";
        
        List<CourseUser> courseUsers = new ArrayList<>();
        courseUsers.addAll(course.getCourseUsers());
        for (CourseUser item : courseUsers){
            if (item.getCourseRole().equals("attendee") && item.getGrade() == 0){
                User user = userDAO.getUserByName(item.getUserName());
            sendMail("lecturer", user.getEmail(), "evaluate the course", text);
            }
        }
    }

}
