
package com.webproject.DAOservices;

import com.webproject.entities.Courses;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CoursesDAO {
    
    SessionFactory sf = new AnnotationConfiguration().configure().buildSessionFactory();

    public void save(Courses course) {
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(course);
        session.flush();
        transaction.commit();
        session.close();
    }
    
    public void update(Courses course) {

        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        session.merge(course);
        session.flush();
        transaction.commit();
        session.close();
    }
    public void delete(Courses course){
        
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(course);
        session.flush();
        transaction.commit();
        session.close();
        
    }
     public List<Courses> getCourses() {

        Session session = sf.openSession();
        List courses = session.createQuery("from Courses ORDER BY course_id").list();
        session.close();
        return courses;
    }

    public List<Courses> getCourses(String category) {

        Session session = sf.openSession();
        List courses = session.createQuery("from Courses where course_category =:cat ORDER BY course_id").setParameter("cat", category).list();
        session.close();
        return courses;
    }
    
    public List<Courses> getCoursesByState(String state) {

        Session session = sf.openSession();
        List courses = session.createQuery("from Courses where course_state =:cat ORDER BY course_id").setParameter("cat", state).list();
        session.close();
        return courses;
    }

    public List<Courses> getCoursesByState(String state, String name) {

        Session session = sf.openSession();
        List courses = session.createQuery("from Courses where course_state =:cat and course_owner=:name ORDER BY course_id").setParameter("cat", state).setParameter("name", name).list();
        session.close();
        return courses;
    }

    public Courses getCourse(int id) {
        Session session = sf.openSession();
        Query query = session.createQuery("from Courses where course_id =:crs_id").setParameter("crs_id", id);
        Courses course = (Courses) query.uniqueResult();
        session.close();
        return course;
    }
    
    
    
}
