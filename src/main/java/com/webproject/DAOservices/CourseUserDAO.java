package com.webproject.DAOservices;

import com.webproject.entities.CourseUser;
import com.webproject.entities.Courses;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CourseUserDAO {
    
    SessionFactory sf = new AnnotationConfiguration().configure().buildSessionFactory();
    
    public void updateCourseUser(CourseUser user) {

        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        session.merge(user);
        session.flush();
        transaction.commit();
        session.close();
    }

   

    

    public CourseUser getCourseUser(Courses course, String name) {
        Session session = sf.openSession();
        Query query = session.createQuery("from CourseUser where course_name =:crs_name and user_name =:name ").setParameter("crs_name", course.getCourseName())
                .setParameter("name", name);
        CourseUser user = (CourseUser) query.uniqueResult();
        session.close();
        return user;
    }

    public List<CourseUser> getSubs(Courses course, String role) {

        Session session = sf.openSession();
        List users = session.createQuery("from CourseUser where course_name =:crs_name and course_role =:role ").setParameter("crs_name", course.getCourseName())
                .setParameter("role", role)
                .list();
        session.close();
        return users;
    }
    
}
