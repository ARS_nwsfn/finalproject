package com.webproject.DAOservices;

import com.webproject.entities.User;
import com.webproject.entities.UserRole;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.context.annotation.Configuration;



@Configuration
public class UserDAO {
    
    SessionFactory sf = new AnnotationConfiguration().configure().buildSessionFactory();
    
    public User getUserByRole(String manager) {
        Session session = sf.openSession();
        User user =(User) session.createQuery("select u from User u, UserRole ur where ur.username = u.username and ur.user_role =:name ").setParameter("name", manager).uniqueResult();
        session.close();
        return user;
    }
    public User getUserByName(String name){
        Session session = sf.openSession();
        User user =(User) session.createQuery("from User where username =:name ").setParameter("name", name).uniqueResult();
        session.close();
        return user;
    }
    
    public List<User> getUsers() {

        Session session = sf.openSession();
        List users = session.createQuery("select u from User u, UserRole ur where ur.username = u.username and ur.user_role =:name ").setParameter("name", "ROLE_USER").list();
        session.close();
        return users;
    }
    
   
    
}
