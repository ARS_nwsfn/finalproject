package com.webproject;

import com.webproject.entities.User;
import com.webproject.entities.UserRole;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

@Service
public class RunJob implements InitializingBean {

    SessionFactory sf = new AnnotationConfiguration().configure().buildSessionFactory();

    public void afterPropertiesSet() {
        Session session = sf.openSession();
        List users = session.createQuery("select u from User u, UserRole ur where ur.username = u.username and ur.user_role =:name ").setParameter("name", "ROLE_USER").list();
        session.close();
        // i did this to run it once. Before it was runing twice.
        if (users.isEmpty()) {
            addUser("user-a");
            addUser("user-b");
            addUser("user-c");
            addLecturer("lecturer-a");
            addLecturer("lecturer-b");
            addManagerDm("dm");
            addManagerKm("km");
        }
    }

    public void addUser(String name) {

        User user = new User();
        user.setUsername(name);
        user.setPassword("123");
        UserRole role1 = new UserRole();
        role1.setUser_role("ROLE_USER");
        role1.setUsername(name);
        Set<UserRole> userRoles = new HashSet<>();
        userRoles.add(role1);
        user.setUserRoles(userRoles);
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(user);
        session.flush();
        transaction.commit();
        session.close();
    }

    public void addLecturer(String name) {

        User user = new User();
        user.setUsername(name);
        user.setPassword("123");
        UserRole role1 = new UserRole();
        role1.setUser_role("ROLE_USER");
        role1.setUsername(name);
        UserRole role3 = new UserRole();
        role3.setUser_role("ROLE_LECTURER");
        role3.setUsername(name);
        Set<UserRole> userRoles = new HashSet<>();
        userRoles.add(role1);
        userRoles.add(role3);
        user.setUserRoles(userRoles);
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(user);
        session.flush();
        transaction.commit();
        session.close();
    }

    public void addManagerDm(String name) {

        User user = new User();
        user.setUsername(name);
        user.setPassword("123");
        UserRole role1 = new UserRole();
        role1.setUser_role("ROLE_USER");
        role1.setUsername(name);
        UserRole role2 = new UserRole();
        role2.setUser_role("ROLE_DEPARTMENT_MANAGER");
        role2.setUsername(name);

        Set<UserRole> userRoles = new HashSet<>();
        userRoles.add(role1);
        userRoles.add(role2);
        user.setUserRoles(userRoles);
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(user);
        session.flush();
        transaction.commit();
        session.close();
    }

    public void addManagerKm(String name) {

        User user = new User();
        user.setUsername(name);
        user.setPassword("123");
        UserRole role1 = new UserRole();
        role1.setUser_role("ROLE_USER");
        role1.setUsername(name);
        UserRole role2 = new UserRole();
        role2.setUser_role("ROLE_KNOWLEDGE_MANAGER");
        role2.setUsername(name);

        Set<UserRole> userRoles = new HashSet<>();
        userRoles.add(role1);
        userRoles.add(role2);
        user.setUserRoles(userRoles);
        Session session = sf.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(user);
        session.flush();
        transaction.commit();
        session.close();
    }
}
