package com.webproject.Controllers;

import com.webproject.entities.CourseUser;
import com.webproject.Services.CourseWork;
import com.webproject.DAOservices.CourseUserDAO;
import com.webproject.entities.Courses;
import com.webproject.email.Notifies;
import com.webproject.DAOservices.CoursesDAO;
import com.webproject.DAOservices.UserDAO;
import com.webproject.Services.Approve;
import com.webproject.entities.State;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/courses")
public class CoursesActionController {

    

    @Autowired
    CourseWork courseWork;
    
    @Autowired
    Approve approve;

    @Autowired
    Notifies nf;
    
    @Autowired
    UserDAO userDAO;
    
    @Autowired
    CoursesDAO courseDAO;
    
    @Autowired
    CourseUserDAO courseUserDAO;
    
    static final Logger LOG =  Logger.getLogger("CoursesActionController");

    // create course
    @RequestMapping(method = RequestMethod.POST, params = {"create"})
    public ModelAndView createCourse(@RequestParam("name") String name, @RequestParam("description") String description,
            @RequestParam("links") String links, @RequestParam("category") String category) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        String err = courseWork.validateUpdateData(name, description, category);
        if (!err.isEmpty()) {
            modelAndView.setViewName("create");
            modelAndView.addObject("auth", auth);
            modelAndView.addObject("error", err);
            return modelAndView;
        }
        Courses course = new Courses(name, auth.getName(), description, links, category);
        courseDAO.save(course);
        LOG.info("course " + name + "was created");
        modelAndView.setViewName("redirect:/courses");
        return modelAndView;
    }

    // update course with subs and atts
    @RequestMapping(method = RequestMethod.POST, params = {"update"})
    public ModelAndView updateCourse(@RequestParam("name") String name, @RequestParam("subs") String subNumber, @RequestParam("description") String description,
            @RequestParam("links") String links, @RequestParam("courseId") String id, @RequestParam("category") String category, @RequestParam(value = "atts", required = false) String attNumber) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        int intId = Integer.parseInt(id);
        String err = courseWork.validateUpdateData(name, description, category);
        int subs = courseWork.validateGrade(subNumber);
        int atts = 0;
        if (attNumber != null) {
            atts = courseWork.validateGrade(attNumber);
        }
        if (!err.isEmpty() || subs == -1 || atts == -1) {
            Courses course = courseDAO.getCourse(intId);
            modelAndView.setViewName("update");
            modelAndView.addObject("auth", auth);
            modelAndView.addObject("course", course);
            modelAndView.addObject("subErr", subs);
            modelAndView.addObject("attErr", atts);
            modelAndView.addObject("error", err);
            return modelAndView;
        }
        courseWork.update(intId, name, description, links, category, subs);
        LOG.info("course " + name + "was updated");
        modelAndView.setViewName("redirect:/courses");
        return modelAndView;
    }

    // delete course
    @RequestMapping(method = RequestMethod.POST, params = {"delete"})
    public ModelAndView deleteCourse(@RequestParam("courseId") String id) {

        ModelAndView modelAndView = new ModelAndView();
        int intId = Integer.parseInt(id);
        Courses course = courseDAO.getCourse(intId);
        nf.notifyManagersDeleted(course);
        LOG.info("course " + course.getCourseName() + "was deleted");
        courseDAO.delete(course);
        modelAndView.setViewName("redirect:/courses");
        return modelAndView;
    }

    // review course
    @RequestMapping(method = RequestMethod.POST, params = {"review"})
    public ModelAndView reviewCourse(@RequestParam("name") String name, @RequestParam("description") String description,
            @RequestParam("links") String links, @RequestParam("courseId") String id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        int intId = courseWork.validateGrade(id);
        Courses course = courseDAO.getCourse(intId);
        if (!course.getOwner().equals(auth.getName())){
            modelAndView.setViewName("action_forbidden");
            return modelAndView;
        }
        courseWork.changeState(course, State.PROPOSAL);
        nf.notifyManager(userDAO.getUserByName(auth.getName()).getEmail(), course, "ROLE_KNOWLEDGE_MANAGER");
        nf.notifyManager(userDAO.getUserByName(auth.getName()).getEmail(), course, "ROLE_DEPARTMENT_MANAGER");
        LOG.info("course " + name + "was reviewed");
        modelAndView.setViewName("redirect:/courses");
        return modelAndView;
    }

    // subscribe and redirect
    @RequestMapping(method = RequestMethod.POST, params = {"sub"})
    public ModelAndView subscribe(@RequestParam("courseId") String id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        int intId = Integer.parseInt(id);
        courseWork.subscribe(intId, auth.getName(), "subscriber");
        Courses course = courseDAO.getCourse(intId);
        List<CourseUser> users = courseUserDAO.getSubs(course, "subscriber");
        if (users.size() >= course.getMinSubs() && course.getState() == State.NEW) {
            courseWork.changeState(course, State.READY);
        }
        LOG.info("user " + auth.getName() + " subscribed to " + course.getCourseName());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/courses");
        return modelAndView;
    }

    // attend and redirect
    @RequestMapping(method = RequestMethod.POST, params = {"attend"})
    public ModelAndView attend(@RequestParam("courseId") String id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        int foo = Integer.parseInt(id);
        courseWork.subscribe(foo, auth.getName(), "attendee");
        ModelAndView modelAndView = new ModelAndView();
        Courses course = courseDAO.getCourse(foo);
        List<CourseUser> users = courseUserDAO.getSubs(course, "subscriber");
        if (users.size() >= course.getMinAtts() && course.getState() == State.OPEN) {
            courseWork.changeState(course, State.READY);
            nf.notifyLectyrerCourseReady(course);
        }
        LOG.info("user " + auth.getName() + " attended to " + course.getCourseName());
        modelAndView.setViewName("redirect:/courses");
        return modelAndView;
    }

    // evaluate and redirect
    @RequestMapping(method = RequestMethod.POST, params = {"evaluate"})
    public ModelAndView evaluate(@RequestParam("courseId") String id, @RequestParam("grade") String grade) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/courses");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        int intId = Integer.parseInt(id);
        Courses course = courseDAO.getCourse(intId);
        int inGrade = courseWork.validateGrade(grade);
        if ((inGrade < 1 || inGrade > 5) && inGrade != -1) {
            inGrade = 10;
            modelAndView.setViewName("evaluate");
            modelAndView.addObject("inGrade", inGrade);
            modelAndView.addObject("course", course);
            modelAndView.addObject("auth", auth);
            return modelAndView;
        }
        if (inGrade == -1) {
            modelAndView.setViewName("evaluate");
            modelAndView.addObject("inGrade", inGrade);
            modelAndView.addObject("course", course);
            modelAndView.addObject("auth", auth);
            return modelAndView;
        }
        boolean cond = courseWork.setGrade(intId, auth.getName(), inGrade);
        if (!cond) {
            inGrade = 11;
            modelAndView.setViewName("evaluate");
            modelAndView.addObject("inGrade", inGrade);
            modelAndView.addObject("course", course);
            modelAndView.addObject("auth", auth);
            return modelAndView;
        }
        LOG.info("user " + auth.getName() + "e valuated course: " + course.getCourseName());
        return modelAndView;
    }

    // approve course
    @RequestMapping(method = RequestMethod.POST, params = {"approve"})
    public ModelAndView approveCourse(@RequestParam("decision") String dec, @RequestParam("reason") String reason,
            @RequestParam("courseId") String id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ArrayList<String> roles = courseWork.getRoles(auth);
        ModelAndView modelAndView = new ModelAndView();
        int intId = Integer.parseInt(id);
        Courses course = courseDAO.getCourse(intId);
        if (dec.equals("Select one")) {
            modelAndView.setViewName("approve");
            modelAndView.addObject("course", course);
            modelAndView.addObject("auth", auth);
            modelAndView.addObject("roles", roles);
            modelAndView.addObject("error", "1");
            return modelAndView;
        }
        approve.approve(auth, course, dec, reason);
        LOG.info("manager " + auth.getName() + " made his decision about course: " + course.getCourseName());
        modelAndView.setViewName("redirect:/courses");
        return modelAndView;
    }

    // start course
    @RequestMapping(method = RequestMethod.POST, params = {"start"})
    public ModelAndView startCourse(@RequestParam("courseId") String id) {

        ModelAndView modelAndView = new ModelAndView();
        int intId = Integer.parseInt(id);
        Courses course = courseDAO.getCourse(intId);
        nf.notifyAttendees("course is started", course);
        courseWork.changeState(course, State.INPROGRESS);
        LOG.info("lecturer " + course.getOwner() + " started the course " + course.getCourseName());
        modelAndView.setViewName("redirect:/courses");
        return modelAndView;
    }

    // finish course
    @RequestMapping(method = RequestMethod.POST, params = {"finish"})
    public ModelAndView finishCourse(@RequestParam("courseId") String id) {

        ModelAndView modelAndView = new ModelAndView();
        int intId = Integer.parseInt(id);
        Courses course = courseDAO.getCourse(intId);
        nf.notifyAttendees("course is finished", course);
        courseWork.changeState(course, State.FINISHED);
        LOG.info("lecturer " + course.getOwner() + " finished the course " + course.getCourseName());
        modelAndView.setViewName("redirect:/courses");
        return modelAndView;
    }

    // finish course
    @RequestMapping(method = RequestMethod.POST, params = {"notify"})
    public ModelAndView notifyForEvaluate(@RequestParam("courseId") String id) {

        ModelAndView modelAndView = new ModelAndView();
        int intId = Integer.parseInt(id);
        Courses course = courseDAO.getCourse(intId);
        nf.notifyAttendeesForEval(course);
        LOG.info("lecturer " + course.getOwner() + " notifies attendees to evaluate the course: " + course.getCourseName());
        modelAndView.setViewName("redirect:/courses");
        return modelAndView;
    }

}
