package com.webproject.Controllers;

import com.webproject.entities.CourseApprove;
import com.webproject.entities.CourseUser;
import com.webproject.Services.CourseWork;
import com.webproject.DAOservices.CourseUserDAO;
import com.webproject.DAOservices.CoursesDAO;
import com.webproject.entities.Courses;
import com.webproject.email.Notifies;
import com.webproject.entities.State;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/courses")
public class CoursesViewController {


    @Autowired
    CourseWork courseWork;

    @Autowired
    Notifies nf;
    
    @Autowired
    CoursesDAO courseDAO;
    
    @Autowired
    CourseUserDAO courseUserDAO;
    

    // show courses
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showCourses(HttpServletRequest request) {
        
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<Courses> courses = courseWork.getCourses(auth);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("roles", courseWork.getRoles(auth));
        modelAndView.addObject("courses", courses);
        modelAndView.setViewName("courses");
        modelAndView.addObject("auth", auth);
        return modelAndView;
    }

    // sort courses
    @RequestMapping(method = RequestMethod.POST, params = {"sort"})
    public ModelAndView sortCourses(@RequestParam("category") String category) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<Courses> courses = courseWork.getCourses(auth);
        courses = courseWork.sort(courses, category);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("roles", courseWork.getRoles(auth));
        modelAndView.addObject("courses", courses);
        modelAndView.setViewName("courses");
        modelAndView.addObject("auth", auth);
        return modelAndView;
    }

    // show details
    @RequestMapping(value = {"/{id}"})
    public ModelAndView showDeatail(@PathVariable("id") int id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("detail");
        Courses course = courseDAO.getCourse(id);
        modelAndView.addObject("course", course);
        modelAndView.addObject("auth", auth);

        if (id == 0) {
            modelAndView.addObject("badId", id);
        }
        return modelAndView;
    }

    // create course page
    @RequestMapping(value = {"/create"})
    public ModelAndView createCoursePage(HttpServletRequest request) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("auth", auth);
        if (!request.isUserInRole("ROLE_LECTURER")) {
            modelAndView.setViewName("create_forbidden");
            return modelAndView;
        }
        modelAndView.setViewName("create");

        return modelAndView;
    }

    //update course page
    @RequestMapping(value = {"/{id}/update"})
    public ModelAndView updateCoursePage(HttpServletRequest request, @PathVariable("id") String id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("update");
        int intId = courseWork.validateGrade(id);
        Courses course = courseDAO.getCourse(intId);
        if ((course != null && !course.getOwner().equals(auth.getName()))) {
            modelAndView.setViewName("update_forbidden");
        }
        if (course != null && !(course.getState() == State.DRAFT || course.getState() == State.REVIEW || course.getState() == State.PROPOSAL)) {
            modelAndView.setViewName("action_forbidden");
        }
        modelAndView.addObject("course", course);
        modelAndView.addObject("auth", auth);
        if (intId == -1) {
            modelAndView.addObject("badId", id);
        }
        return modelAndView;
    }

    @RequestMapping(value = {"/{id}/delete"})
    public ModelAndView deleteCoursePage(HttpServletRequest request, @PathVariable("id") String id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("delete");
        int intId = courseWork.validateGrade(id);
        Courses course = courseDAO.getCourse(intId);
        if ((course != null && !course.getOwner().equals(auth.getName()))) {
            modelAndView.setViewName("action_forbidden");
        }
        if (course != null && !(course.getState() == State.DRAFT || course.getState() == State.REJECT)) {
            modelAndView.setViewName("action_forbidden");
        }
        modelAndView.addObject("course", course);
        modelAndView.addObject("auth", auth);
        if (intId == -1) {
            modelAndView.addObject("badId", id);
        }
        return modelAndView;
    }

    // subscribtion form
    @RequestMapping(value = {"/{id}/subscribe"})
    public ModelAndView subscriptionForm(@PathVariable("id") String id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("subscribe");
        int intId = courseWork.validateGrade(id);
        Courses course = courseDAO.getCourse(intId);
        modelAndView.addObject("course", course);
        modelAndView.addObject("auth", auth);
        if (intId == -1) {
            modelAndView.addObject("badId", id);
        }
        if (!(course.getState() == State.READY || course.getState() == State.NEW || course.getState() == State.OPEN)) {
            modelAndView.setViewName("action_forbidden");
        }
        return modelAndView;
    }

    // attendance form
    @RequestMapping(value = {"/{id}/attend"})
    public ModelAndView attendanceForm(@PathVariable("id") String id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("attend");
        int intId = courseWork.validateGrade(id);
        Courses course = courseDAO.getCourse(intId);
        modelAndView.addObject("course", course);
        modelAndView.addObject("auth", auth);
        if (intId == -1) {
            modelAndView.addObject("badId", id);
        }
        if (!(course.getState() == State.READY || course.getState() == State.OPEN)) {
            modelAndView.setViewName("action_forbidden");
        }
        return modelAndView;
    }

    // evaluation form
    @RequestMapping(value = {"/{id}/evaluate"})
    public ModelAndView evaluationForm(@PathVariable("id") String id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("evaluate");
        int intId = courseWork.validateGrade(id);
        Courses course = courseDAO.getCourse(intId);
        modelAndView.addObject("course", course);
        modelAndView.addObject("auth", auth);
        if (intId == -1) {
            modelAndView.addObject("badId", id);
        }
        if (course.getState() != State.FINISHED) {
            modelAndView.setViewName("action_forbidden");
        }
        return modelAndView;
    }

    // participants form
    @RequestMapping(value = {"/{id}/participants"})
    public ModelAndView participants(@PathVariable("id") String id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("participants");
        int intId = courseWork.validateGrade(id);
        Courses course = courseDAO.getCourse(intId);
        List<CourseUser> subUsers = courseUserDAO.getSubs(course, "subscriber");
        List<CourseUser> attUsers = courseUserDAO.getSubs(course, "attendee");
        modelAndView.addObject("course", course);
        modelAndView.addObject("subs", subUsers);
        modelAndView.addObject("atts", attUsers);
        modelAndView.addObject("auth", auth);
        if (intId == -1) {
            modelAndView.addObject("badId", id);
        }
        if (course.getState() != State.READY || course.getState() != State.INPROGRESS) {
            modelAndView.setViewName("action_forbidden");
        }

        return modelAndView;
    }

    // send to review
    @RequestMapping(value = {"/{id}/review"})
    public ModelAndView review(@PathVariable("id") String id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("auth", auth);
        modelAndView.setViewName("redirect:/courses");
        int intId = courseWork.validateGrade(id);
        Courses course = courseDAO.getCourse(intId);

        if (intId == -1) {
            modelAndView.setViewName("action_forbidden");
            return modelAndView;
        }
        if (course.getState() != State.DRAFT) {
            modelAndView.setViewName("action_forbidden");
            return modelAndView;
        }
        courseWork.changeState(course, State.REVIEW);
        return modelAndView;
    }

    // approve form
    @RequestMapping(value = {"/{id}/approve"})
    public ModelAndView approveCourse(@PathVariable("id") String id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("approve");
        int intId = courseWork.validateGrade(id);
        Courses course = courseDAO.getCourse(intId);
        if (course != null && course.getState() != State.PROPOSAL) {
            modelAndView.setViewName("action_forbidden");
        }
        ArrayList<String> roles = courseWork.getRoles(auth);
        if (!(roles.contains("ROLE_DEPARTMENT_MANAGER") || roles.contains("ROLE_KNOWLEDGE_MANAGER"))) {
            modelAndView.setViewName("action_forbidden");
        }
        CourseApprove approval = course.getApprove();
        if (approval == null) {
            approval = new CourseApprove();
        }
        modelAndView.addObject("approval", approval);
        modelAndView.addObject("course", course);
        modelAndView.addObject("auth", auth);
        modelAndView.addObject("roles", roles);
        if (intId == -1) {
            modelAndView.addObject("badId", id);
        }
        return modelAndView;
    }

// start course page
    @RequestMapping(value = {"/{id}/start"})
    public ModelAndView startCoursePage(@PathVariable("id") String id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("start");
        int intId = courseWork.validateGrade(id);
        Courses course = courseDAO.getCourse(intId);
        if ((course != null && !course.getOwner().equals(auth.getName()))) {
            modelAndView.setViewName("action_forbidden");
        }
        if (course != null && course.getState() != State.READY) {
            modelAndView.setViewName("action_forbidden");
        }
        modelAndView.addObject("course", course);
        modelAndView.addObject("auth", auth);
        if (intId == -1) {
            modelAndView.addObject("badId", id);
        }
        return modelAndView;
    }

    // finish course page
    @RequestMapping(value = {"/{id}/finish"})
    public ModelAndView finishCoursePage(@PathVariable("id") String id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("finish");
        int intId = courseWork.validateGrade(id);
        Courses course = courseDAO.getCourse(intId);
        if ((course != null && !course.getOwner().equals(auth.getName()))) {
            modelAndView.setViewName("action_forbidden");
        }
        if (course != null && course.getState() != State.INPROGRESS) {
            modelAndView.setViewName("action_forbidden");
        }
        modelAndView.addObject("course", course);
        modelAndView.addObject("auth", auth);
        if (intId == -1) {
            modelAndView.addObject("badId", id);
        }
        return modelAndView;
    }

    // notify page about evaluation
    @RequestMapping(value = {"/{id}/notify"})
    public ModelAndView notifyCoursePage(@PathVariable("id") String id) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("notify");
        int intId = courseWork.validateGrade(id);
        Courses course = courseDAO.getCourse(intId);
        if ((course != null && !course.getOwner().equals(auth.getName()))) {
            modelAndView.setViewName("action_forbidden");
        }
        if (course != null && course.getState() != State.FINISHED) {
            modelAndView.setViewName("action_forbidden");
        }
        modelAndView.addObject("course", course);
        modelAndView.addObject("auth", auth);
        if (intId == -1) {
            modelAndView.addObject("badId", id);
        }
        return modelAndView;
    }

}
