
package com.webproject.Controllers;

import com.webproject.Services.CourseWork;
import com.webproject.entities.Courses;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/mycourses")
public class MyCoursesController {
    


    @Autowired
    CourseWork courseWork;

    // show courses
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showMyCourses(HttpServletRequest request) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<Courses> courses = courseWork.getUserCourses(auth);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("courses", courses);
        modelAndView.setViewName("mycourses");
        modelAndView.addObject("auth", auth);
        return modelAndView;
    }
    // sort courses
    @RequestMapping(method = RequestMethod.POST, params = {"sort"})
    public ModelAndView sortMyCourses(@RequestParam("category") String category) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<Courses> courses = courseWork.getCourses(auth);
        courses = courseWork.sort(courses, category);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("roles", courseWork.getRoles(auth));
        modelAndView.addObject("courses", courses);
        modelAndView.setViewName("courses");
        modelAndView.addObject("auth", auth);
        modelAndView.setViewName("courses");
        return modelAndView;
    }
    
}
