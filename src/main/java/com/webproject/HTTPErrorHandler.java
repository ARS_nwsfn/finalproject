package com.webproject;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HTTPErrorHandler {

    @RequestMapping(value = "/404")
    public String error404(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); 
        if (name == "anonymousUser") {
            return "/login";
        }
        return "redirect:/courses";
    }

    @RequestMapping(value = "/400")
    public String error400(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName(); 
        if (name == "anonymousUser") {
            return "/login";
        }
        return "redirect:/courses";
    }

}
