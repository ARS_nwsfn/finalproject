/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webproject;

import com.webproject.Services.CourseWork;
import com.webproject.entities.Courses;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class CourseWorkTest {
    
    public CourseWorkTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of validate method, of class CourseWork.
     */
    @Test
    public void testValidate() {
        System.out.println("validate");
        String name = "";
        String desc = "";
        String category = "";
        CourseWork instance = new CourseWork();
        String expResult = "135";
        String result = instance.validateUpdateData(name, desc, category);
        assertEquals(expResult, result);
    }



    /**
     * Test of validateGrade method, of class CourseWork.
     */
    @Test
    public void testValidateGrade() {
        System.out.println("validateGrade");
        String grade = "";
        CourseWork instance = new CourseWork();
        int expResult = -1;
        int result = instance.validateGrade(grade);
        assertEquals(expResult, result);
    }


    /**
     * Test of sort method, of class CourseWork.
     */
    @Test
    public void testSort() {
        System.out.println("sort");
        List<Courses> crs = new ArrayList<>();
        String category = "";
        CourseWork instance = new CourseWork();
        List<Courses> expResult = new ArrayList<>();
        List<Courses> result = instance.sort(crs, category);
        assertEquals(expResult, result);
    }



    
}
